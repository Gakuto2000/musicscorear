using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePages : MonoBehaviour
{
    [SerializeField] MeshRenderer GoNext;
    [SerializeField] MeshRenderer GoBack;
    public void ShowPageUI()
    {
        GoNext.enabled = true;
        GoBack.enabled = true;
        Debug.Log("showpageUI");
    }

    public void HidePageUI()
    {
        GoNext.enabled = false;
        GoBack.enabled = false;
        Debug.Log("hidepageUI");
    }
}
