using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScorePos : MonoBehaviour
{
    [SerializeField] private GameObject ScoreManager;
    [SerializeField] private GameObject MainCamera;

    private float diflong;
    private Vector3 dif;
    private Quaternion firstQ;
    private Vector3 firstScale;
    // Start is called before the first frame update
    void Start()
    {
        dif = ScoreManager.transform.position - MainCamera.transform.position;
        diflong = Vector3.Distance(ScoreManager.transform.position,MainCamera.transform.position);

        firstQ = ScoreManager.transform.rotation;

        firstScale = ScoreManager.transform.localScale;
    }

    public void ResetPosition()
    {
        Debug.Log("リセットポジション");

        //ScoreManager.transform.position = MainCamera.transform.position + dif;
        ScoreManager.transform.position = MainCamera.transform.position + MainCamera.transform.forward * diflong;
        //ScoreManager.transform.rotation = firstQ;
        ScoreManager.transform.LookAt(MainCamera.transform.position) ;//-90suru 
        //ScoreManager.transform.Rotate(-90.0f, 180.0f, 0.0f);
        ScoreManager.transform.Rotate(0.0f, 180.0f, 0.0f);

        ScoreManager.transform.localScale = firstScale;
    }
}
