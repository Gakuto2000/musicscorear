using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Track : MonoBehaviour
{
    [SerializeField] GameObject scoreManager;
    [SerializeField] GameObject mainCamera;

    private SolverHandler solverHandler;
    private RadialView radialView;
    private FollowMeToggle followMe;

    private bool switchOnOff = false;

    //private Vector3 dif;
    float diflong;
    Vector3 CameraToScore;

    // Start is called before the first frame update
    void Start()
    {
        solverHandler = scoreManager.GetComponent<SolverHandler>();
        radialView = scoreManager.GetComponent<RadialView>();
        followMe = scoreManager.GetComponent<FollowMeToggle>();

        //本当はないけどテスト、後ろだをされるのはどうして〜？
      //  TrackON();
    }

    void Update()
    {
        if(switchOnOff == true)
        {
            TrackCamera();
        }
    }


    //ONの時は楽譜を指で動かす事はできない。楽譜選択パネルのように頭に大して追従する
    //OFFの時は大きさを変えたり動かしたりできるが、座標は固定されてしまう

    public void TrackSwitch()
    {
        if (switchOnOff == false)
        {
            Debug.Log("トラックオン");
            diflong = Vector3.Distance(scoreManager.transform.position,mainCamera.transform.position);
            CameraToScore = (scoreManager.transform.position - mainCamera.transform.position).normalized;
            switchOnOff = true;
        }
        else
        {
            Debug.Log("トラックオフ");
            switchOnOff = false;
        }
    }

    private void TrackCamera()
    {
        //座標は簡単に出せる
        //mainCamera.transform.forwardに当たる部分が、カメラから楽譜への単位ベクトルにする

        //scoreManager.transform.position = mainCamera.transform.position + CameraToScore * diflong;
         scoreManager.transform.position = mainCamera.transform.position + mainCamera.transform.forward * diflong;

        //回転
        scoreManager.transform.LookAt(mainCamera.transform.position);
        scoreManager.transform.Rotate(0.0f, 180.0f, 0.0f);

    }
}
