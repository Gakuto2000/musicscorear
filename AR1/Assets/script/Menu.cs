using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] GameObject MenuPlane;
    [SerializeField] GameObject ScoreManager;

    [SerializeField] Score score;

    //楽譜を開くと座標が固定される
    [SerializeField] RadialView radiaview;


    //メニューを開く
   public void OpenMenu()
   {
        if (MenuPlane.activeInHierarchy == true)
        {
            //メニューが開いているときは、メニューを閉じる
            MenuPlane.SetActive(false);
        }
        else
        {
            //楽譜が開いているときは、メニューを見せる
            MenuPlane.SetActive(true);
            ScoreManager.SetActive(false);

            //楽譜リストは常に顔の前に表示される
            if(radiaview != null)
            radiaview.enabled = true;
        }

        Debug.Log("オープンメニュー");
   }

    //楽譜を表示する
    public void ShowSelectScore(int scoreNum)
    {
        //楽譜を表示する
        MenuPlane.SetActive(false);
        ScoreManager.SetActive(true);

        //指定された楽譜をセットする
        score.ViewScore(scoreNum);

        //楽譜の座標を固定する
     /*   if (radiaview != null)
            radiaview.enabled = false;*/

        Debug.Log(scoreNum.ToString() + "番の " + "楽譜を表示");
    }
}
