using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogTest : MonoBehaviour
{


    public void LogTouch()
    {
        Debug.Log(gameObject.name +  "タッチされました");
    }

    public void ShowLog(string message)
    {
        Debug.Log(message);
    }

}
