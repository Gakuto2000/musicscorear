using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuIcon : MonoBehaviour
{
    [SerializeField] GameObject scoreManager;
    private Vector3 firstposition;
    // Start is called before the first frame update
    void Start()
    {
        firstposition =  scoreManager.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = scoreManager.transform.position - firstposition;
    }
}
