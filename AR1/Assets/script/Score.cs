using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private List<SpriteList> ScoreList; //すべての楽譜が収納されているリスト

    [System.SerializableAttribute]
    public class SpriteList {
        public List<Sprite> List;

        public SpriteList(List<Sprite> list)
        {
            List = list;
        }
    }

    private List<Sprite> ScoreSprites;//表示中の楽譜
    [SerializeField] private int ScoreNum;//表示中の楽譜の番号　1;カエルのうた(1)　2:メヌエット(2) 3:バラード(4) 4:エリーゼのために(5)

    public int LeftPageNum; //右側のページ番号 ページ数は１から数える

    [SerializeField] private SpriteRenderer LeftScore, RightScore, CentorScore;

    //ページ数を表示するための変数
    [SerializeField] private TextMesh ScorePageText;

    // Start is called before the first frame update
    void Start()
    {
       // ViewScore(3);
    }

    public void ViewScore(int scoreNum)
    {
        //表示中の楽譜を更新
        ScoreNum = scoreNum;
        ScoreSprites = ScoreList[ScoreNum].List;

        //現在のページ番号を格納
        LeftPageNum = 1;

        SetScoreSprite();
        SetPageNumberText();
    }

    public void GoNextPages()
    {
        if(LeftPageNum + 2 <= ScoreSprites.Count)
        {
            LeftPageNum += 2;
            SetScoreSprite();
            SetPageNumberText();
        }
    }

    public void GoBackPages()
    {
        if(LeftPageNum - 2 > 0)
        {
            LeftPageNum -= 2;
            SetScoreSprite();
            SetPageNumberText();
        }


    }

    //楽譜に画像をセットする
    private void SetScoreSprite()
    {
        if(LeftPageNum == ScoreSprites.Count)
        {
            LeftScore.gameObject.SetActive(false);
            RightScore.gameObject.SetActive(false);
            CentorScore.gameObject.SetActive(true);

            //最後の1ページの場合は1枚のみ表示
            CentorScore.sprite = ScoreSprites[LeftPageNum - 1];
        }
        else
        {
            LeftScore.gameObject.SetActive(true);
            RightScore.gameObject.SetActive(true);
            CentorScore.gameObject.SetActive(false);

            LeftScore.sprite = ScoreSprites[LeftPageNum - 1];
            RightScore.sprite = ScoreSprites[LeftPageNum];
        }
    }

    //今のページ数を表示する
    private void SetPageNumberText()
    {
        //最初は１からのほうが分かりやすいような気がする

        ScorePageText.text = LeftPageNum.ToString() + "/" + ScoreSprites.Count.ToString();
    }

}
