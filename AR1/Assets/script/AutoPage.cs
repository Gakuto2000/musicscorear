using System.Collections;
using System.Collections.Generic;
//using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class AutoPage : MonoBehaviour
{
    [SerializeField] GameObject mainCamera;
    [SerializeField] Score score;

    [SerializeField] float TimeToGo = 2;
    [SerializeField] float distance = 100; // 飛ばす&表示するRayの長さ

    [SerializeField] Slider NextSlider;
    [SerializeField] Slider BackSlider;

    [SerializeField] GameObject CentorPoint;

    private float timer = 0;
    private bool IsCounting = true;

    private bool OnOff = false;

    // Update is called once per frame
    void Update()
    {
        if(OnOff == false)
        {
            return;
        }

        //Debug.Log(timer);

        Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        RaycastHit hit = new RaycastHit();


        if (Physics.Raycast(ray, out hit, distance))
        {
            if (CentorPoint.activeSelf == false)
            {
                CentorPoint.SetActive(true);
            }

            //このままではボタンのうえに残ってしまうので、頻繁に出し消ししたほうが良い。
            CentorPoint.transform.position = hit.point;

            string name = hit.collider.gameObject.name;

            Debug.Log(hit.point);
            
            if(name == "NextCol")
            {
                if (IsCounting == true)
                {
                    timer += Time.deltaTime;
                    NextSlider.value = timer/ TimeToGo;
                }

                if(timer >= TimeToGo)
                {
                    score.GoNextPages();
                    IsCounting = false;
                    timer = 0; //本当はカウント自体を一度やめてほしい
                    NextSlider.value = timer / TimeToGo;
                }
                
            }
            else if (name == "BackCol")
            {
                if (IsCounting == true)
                {
                    timer += Time.deltaTime;
                    BackSlider.value = timer / TimeToGo;
                }
                else
                {
                    //Debug.Log("連続でページめくりはできません");
                }

                if (timer >= TimeToGo)
                {
                    score.GoBackPages();
                    IsCounting = false;
                    timer = 0; //本当はカウント自体を一度やめてほしい
                    BackSlider.value = timer / TimeToGo;
                }
            }
            else
            {
                if (IsCounting == false)
                {
                    IsCounting = true;
                }

                if (timer > 0)
                {
                    timer = 0;
                    NextSlider.value = timer / TimeToGo;
                    BackSlider.value = timer / TimeToGo;
                    //Debug.Log("ページのカウントをリセット");
                }
            }
        }
        else
        {
            if (CentorPoint.activeSelf == true)
            {
                CentorPoint.SetActive(false);
            }

            //何も認識していない時
            if (IsCounting == false)
            {
                IsCounting = true;
            }

            if (timer > 0)
            {
                timer = 0;
                NextSlider.value = timer / TimeToGo;
                BackSlider.value = timer / TimeToGo;
                //Debug.Log("ページのカウントをリセット");
            }
        }
    }

    public void SwitchAuto()
    {
        if(OnOff == false)
        {
            AutoPagesON();
        }
        else
        {
            AutoPagesOFF();
        }
    }

    public void AutoPagesON()
    {
        //スタイダ―を表示
        NextSlider.gameObject.SetActive(true);
        BackSlider.gameObject.SetActive(true);

        CentorPoint.SetActive(true);

        //Updateを動かす
        OnOff = true;
    }

    public void AutoPagesOFF() 
    {
        //スタイダ―を非表示
        NextSlider.gameObject.SetActive(false);
        BackSlider.gameObject.SetActive(false);

        CentorPoint.SetActive(false);

        //Updateを停止する
        OnOff = false;
    }
}
